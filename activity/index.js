/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function genInfo() {
        
        let fullName = prompt("What is your name?"); 
        let age = prompt("How old are you?"); 
        let address = prompt("Where do you live?");
        alert("Thank you for your input");
    
        console.log("Hello, " + fullName); 
        console.log("You are " + age + "years old."); 
        console.log("You live in " + address); 
    };
    
    genInfo();
    

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/  function favBands() {
    alert("Please enter your top 5 favorite bands");
    let band1 = prompt("First"); 
    let band2 = prompt("Second"); 
    let band3 = prompt("Third");
    let band4 = prompt("Fourth");
    let band5 = prompt("Fifth")

    console.log("1. " + band1); 
    console.log("2. " + band2); 
    console.log("3. " + band3);
    console.log("4. " + band4);
    console.log("5. " + band5); 
};

favBands();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function favMovies() {
        let movie1 = ("Big Hero 6"); 
        let movie2 = ("A walk to remember"); 
        let movie3 = ("Mulan");
        let movie4 = ("Curious case of Benjamin  Button");
        let movie5 = ("Along with the gods");
    
        console.log(movie1);
        console.log("Rotten Tomatoes Rating: 90%"); 
        console.log(movie2); 
        console.log("Rotten Tomatoes Rating: 27%");
        console.log(movie3);
        console.log("Rotten Tomatoes Rating: 86%");
        console.log(movie4);
        console.log("Rotten Tomatoes Rating: 71%");
        console.log(movie5); 
        console.log("Rotten Tomatoes Rating: 67%");
    };
    
    favMovies();
    
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printFriends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();